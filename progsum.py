import sys
import openai
import subprocess
import logging
from argparse import ArgumentParser

# For token counting
import tiktoken

# For url parsing
import requests
from bs4 import BeautifulSoup

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')


def yes_no_prompt(prompt):
    while True:
        user_input = input(prompt + " (y/n): ").lower().strip()
        if user_input == 'y':
            return True
        elif user_input == 'n':
            return False
        else:
            print("Invalid input. Please enter 'y' or 'n'.")


def parse_url(url):
    response = requests.get(url)

    if response.status_code == 200:
        soup = BeautifulSoup(response.content, "html.parser")
        text = soup.get_text(strip=True)
        return text
    else:
        logging.critical(f"Failed to download URL: {url}. Status code: {response.status_code}")
        exit(1)


def num_tokens_from_string(string: str) -> int:
    """Returns the number of tokens in a text string."""
    encoding = tiktoken.encoding_for_model("gpt-4")
    num_tokens = len(encoding.encode(string))
    return num_tokens


def progressive_summary(text, query):
    def make_prompt(query, current_summary, next_chunk):
        return f"""
You answer a query about a large text chunk by chunk.
The query that you answer is: "{query}"

The answer to the query about the previous chunks of the large text is as follows:
\"\"\"
{current_summary}
\"\"\"

Update the above answer to the query as you see fit.
Consider what new information the next chunk of text adds to the answer of the query.
Consider how the answer needs to be updated with new information from the next chunk.

Next chunk: \"\"\"
{next_chunk}
\"\"\"
"""

    paragraphs = text.split("\n\n")
    current_summary = ""
    empty_prompt_tokens = num_tokens_from_string(make_prompt(query, "", ""))

    logging.info("Starting summary.")

    while len(paragraphs) > 0:
        next_chunk = ""
        next_chunk_tokens = 0

        logging.info("Computing next chunk.")
        # Compute next chunk.
        while (len(paragraphs) > 0) and (next_chunk_tokens < 6000 - empty_prompt_tokens):
            next_paragraph = paragraphs.pop(0)  # Inefficient but ok for us?
            next_paragraph_tokens = num_tokens_from_string(next_paragraph)
            if next_chunk_tokens + next_paragraph_tokens <= 6000:
                next_chunk += next_paragraph + "\n\n"
                next_chunk_tokens += next_paragraph_tokens
            elif next_paragraph_tokens >= 4000:
                part_one = next_paragraph[:2000]
                part_two = next_paragraph[2000:]
                next_chunk += part_one
                next_chunk_tokens += num_tokens_from_string(part_one)
                paragraphs = [part_two] + paragraphs

        prompt = make_prompt(query, current_summary, next_chunk)
        logging.info(f"Querying GPT with: {query}")

        completion = openai.ChatCompletion.create(model="gpt-4", messages=([{"role": "user", "content": prompt}]))
        current_summary = completion.choices[0].message.content

        logging.info(f"Got the following response: {current_summary}")

    return current_summary


def get_api_key():
    result = subprocess.run(["pass", "openai-api-key"], stdout=subprocess.PIPE, text=True)
    return result.stdout.strip()

openai.api_key = get_api_key()

argparser = ArgumentParser(description="A progressive summarizer using OpenAI's GPT")

argparser.add_argument('query', nargs='*', default=None, type=str, help="The query to be carried out on the input data.")
argparser.add_argument('-u', '--url', type=str, help="Url to be parsed.")
args = argparser.parse_args()
query = args.query
url = args.url

text = None

if query == None:
    query = "Summarize the text."

if url != None:
    logging.info("Getting url.")
    text = parse_url(url)
else:
    logging.info("Reading text from stdin.")
    text = sys.stdin.read()

logging.info("Calculating number of tokens.")
tokens = num_tokens_from_string(text)

logging.info(f"I will request a summary for {tokens} tokens.")
# if not yes_no_prompt(f"I will request a summary for {tokens} tokens. Continue?"):
#     exit(0)

summary = progressive_summary(text, query)

print(summary)
