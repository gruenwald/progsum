# progsum

A progressive summarizer using OpenAI's ChatGPT. Run any query on a large text or web document through ChatGPT. The document is divided into chunks. The query is then answered for the first chunk, then the answer updated on the second chunk and so on. Provide the document either via stdin or via an url from which to download it. If no query is provided, the document is summarized.

usage: progsum.py [-h] [-u URL] [query ...]

Examples: progsum.py "List all suggestions for kids' presents in this website" -u "https://news.ycombinator.com/item?id=35507748"
  
